package com.grishin;

import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;

public class ServerChat {
    public static void main(String[] args) throws Exception {
        HashMap<String, Socket> users = new HashMap<>();
        ServerSocket server = new ServerSocket(1234);
        System.out.println("Соеденение установлено");
        while (true) {
            Socket client = server.accept();
            ClientFromThread cft = new ClientFromThread(client);
            String name = cft.getUserName();
            users.put(name, client);
            cft.giveUsersMap(users);
            System.out.println("SERVER: Новый пользователь на сервере " + name);
        }

    }

    static class ClientFromThread extends Thread {
        private Socket client;
        private Scanner fromClient;
        private PrintWriter toClient;
        private String userName;
        HashMap<String, Socket> users;

        public ClientFromThread(Socket c) throws Exception {
            client = c;
            fromClient = new Scanner(client.getInputStream());
            toClient = new PrintWriter(client.getOutputStream(), true);
            userName = getUser();
            start();
        }

        public void giveUsersMap(HashMap<String, Socket> users) {
            this.users = users;
        }

        public String getUserName() {
            return userName;
        }

        private String getUser() {
            String s = "";
            while (s.length() < 1) {
                s = fromClient.nextLine().trim();
            }
            toClient.println("Привет " + s + "!");
            return s.toUpperCase();
        }

        @Override
        public void run() {
            String s;
            String toUser;
            String msg;

            while (fromClient.hasNext()) {
                s = fromClient.nextLine().trim();
                if (s.equalsIgnoreCase("END")) break;

                for (int i = 0; i < s.length(); i++) {
                    if (s.charAt(i) == '-') {
                        toUser = s.substring(0, i).trim().toUpperCase();
                        msg = s.substring(i + 1).trim();
                        Socket client = users.get(toUser);
                        try {
                            ClientToThread ctt = new ClientToThread(client);
                            ctt.sendMsg(msg, toUser);
                            ctt.start();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    }
                    if (s.charAt(i) == '='){

                    }
                    if ((i + 1) == s.length()) {
                        toClient.println("Ошибка! Для отправки сообщений необходимо поставить (-) перед именем пользователя ");
                    }
                }
            }
            try {
                fromClient.close();
                toClient.close();
                client.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    static class ClientToThread extends Thread {
        private Socket client;
        private PrintWriter toClient;
        private String msg;

        public ClientToThread(Socket c) throws Exception {
            client = c;
            toClient = new PrintWriter(client.getOutputStream(), true);
        }

        public void sendMsg(String msg, String userName) {
            this.msg = userName + ": " + msg;
        }

        @Override
        public void run() {
            try {
                toClient.println(msg);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }
}
