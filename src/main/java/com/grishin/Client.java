package com.grishin;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws Exception {
        String line;
        Socket server = new Socket("localhost", 1234);
        System.out.println("Соединение установлено: " + server.getInetAddress());
        System.out.println("Введите ваше имя");
        BufferedReader fromServer = new BufferedReader(
                new InputStreamReader(server.getInputStream()));
        PrintWriter toServer = new PrintWriter(server.getOutputStream(), true);
        BufferedReader input = new BufferedReader(
                new InputStreamReader(System.in));
        while ((line = input.readLine()) != null) {

            toServer.println(line);
            System.out.println(fromServer.readLine());
        }
        fromServer.close();
        toServer.close();
        input.close();
        server.close();
    }
}
